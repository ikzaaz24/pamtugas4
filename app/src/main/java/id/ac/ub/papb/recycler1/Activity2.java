package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        Intent intent = getIntent();
        String nim = intent.getStringExtra("NIM");
        String nama = intent.getStringExtra("Nama");

        // Menampilkan data pada TextView di Activity2
        TextView tvNim = findViewById(R.id.tvNim2);
        TextView tvNama = findViewById(R.id.tvNama2);

        tvNim.setText("NIM: " + nim);
        tvNama.setText("Nama: " + nama);
    }
}
